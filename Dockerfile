FROM node:alpine
COPY . .
COPY .npmrc .npmrc

RUN npm install
RUN rm -rf .npmrc
CMD [ "node", "index.js" ]